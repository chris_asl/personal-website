
$( document ).ready(function() {
    var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    setJumbotronSize(viewportHeight);

    var navbarHeight = parseInt($(".navbar").outerHeight());
    addMarginBottom("#introduction", viewportHeight, true, navbarHeight);
    addMarginBottom("#projects", viewportHeight, true, navbarHeight);
    var footerHeight = parseInt($(".footer").outerHeight(true));
    addMarginBottom("#contact", viewportHeight, true, navbarHeight + footerHeight);

	$("[data-toggle='tooltip']").tooltip();
	$('.welcome-msg-title').delay(0).fadeIn(2500);
	$('.welcome-msg').delay(700).fadeIn(2000);
	$(".attention").delay(900).toggle( "bounce", { distance: 35, times: 3 }, 800 );
});

// ----------------------------------------------------------------------------------
// Resize/setting height methods
function setJumbotronSize(viewportHeight) {
    $("#welcome").css('height', viewportHeight);
	// Setting the attributes of actual jumbotron to center vertically.
    var jumbotronTargetHeight;
    jumbotronTargetHeight =  viewportHeight/2;  // Vertically center it.
    $('#content')
        .css({ 'height': jumbotronTargetHeight + 'px', 'margin-top': (jumbotronTargetHeight/2) + 'px'});
    // TODO check with multiple heights
}

function addMarginBottom(sectionId, viewportHeight, shouldAddBottomMargin, extraHeight) {
    var sectionHeader = $(sectionId);

    if (!shouldAddBottomMargin)
        $(sectionId).css('height', viewportHeight);
    else {
        // Calculate section header's height so as to subtract it from viewportHeight.
        var sectionHeaderViewedHeight = parseFloat(sectionHeader.outerHeight(false));
        // Section header's top is hidden by navbar by 10px when following the inner links.
        // That's why we need to reduce its height by 10px, since these are hidden at the view port.
        sectionHeaderViewedHeight -= 10;

        var sectionContentId = sectionId + "-content";
        var sectionCurrentHeight = parseFloat($(sectionContentId).outerHeight(true));

        var sectionTotalHeight = sectionHeaderViewedHeight + sectionCurrentHeight;

        // Only add margin-bottom if total height is smaller that viewport height.
        if (sectionTotalHeight < viewportHeight) {
            var marginBottomValue =
                parseFloat(viewportHeight - sectionTotalHeight - extraHeight);
            $(sectionContentId).css('margin-bottom', marginBottomValue);
        }
    }
    return viewportHeight;
}
// ----------------------------------------------------------------------------------


// ----------------------------------------------------------------------------------
// Scrolling and waypoints.
var waypointOffset = 150;
var navBarActiveCSSClass = "active";

$('a.scroll').click(function(event) {
    var scrollToOffset = 40;
	event.preventDefault();
	// Get target and its offset
	var fullUrl = this.href, parts = fullUrl.split('#'), target = parts[1],
        targetOffset = $('#'+target).offset(), targetTop = targetOffset.top - scrollToOffset;

	$('html, body').animate({scrollTop:targetTop}, 800);

	/* Take into account the special case in which the brand
	    is clicked, and we are already on top
	    Remove active class on any li when an anchor is clicked */
	if (window.pageYOffset < window.height)
		$('#my-collapsed-menu').children().children().removeClass();

	if (($('.navbar-toggle').is(':visible')) && ($(this).parent().is('li') && (target != "welcome")))
		setTimeout("$('button.navbar-toggle').click()", 1000);
});

$(document).on('click',function(e){
	// Close dropdown (if opened) if the user clicks somewhere else.
	if (($('.navbar-toggle').is(':visible')) && ($('#my-collapsed-menu').hasClass('in'))) {
		// Prevent the toggling if a navbar link was clicked
		if (($(e.target).closest('#my-collapsed-menu').length == 0))
		// No such element found, close to the target
			$('button.navbar-toggle').click();
    	}
});

// Disable showing the Back to top on when welcome div is shown
$('#welcome').waypoint(function(direction) {
	if (direction === "up") {
        $('div.follow-scroll').fadeOut(500);
        $('#my-collapsed-menu').children().children().removeClass();
    }
}, {
	offset: function() {
		return -$(this).height() -($(this).height() / 2);
	}
});


// ---------------------------------------------------------------------------------------------------------------------
// Enable active class of the correct navbar link

// --------------------------------------------------------------------------
// Waypoint for welcome view.

// Going down
$('#welcome-content').waypoint(function(direction) {
    if (direction === "down") {
        // Remove active class
        $('#my-collapsed-menu').children().children().removeClass();
        // Add proper active class
        $('a[href$="welcome"]').parent().addClass(navBarActiveCSSClass);
        $('div.follow-scroll').fadeIn(500);
    }
}, {
    offset: function() {
        return $('#welcome-content span').height();
    }
});

// Going up
$('#welcome').waypoint(function(direction) {
    if (direction === "up") {
        // Remove active class
        $('#my-collapsed-menu').children().children().removeClass();
        // Add proper active class
        $('a[href$="welcome"]').parent().addClass(navBarActiveCSSClass);
    }
}, {
    offset: function() {
        return $('#welcome-content span').height() - waypointOffset;
    }
});

// --------------------------------------------------------------------------
// Waypoint for introduction

// Going down
$('#introduction').waypoint(function(direction) {
	if (direction === "down") {
		// Remove active class
        $('.my-navbar-brand-list > li').removeClass();
		$('#my-collapsed-menu').children().children().removeClass();
		// Add proper active class
		$('a[href$="introduction"]').parent().addClass(navBarActiveCSSClass);
		$('div.follow-scroll').fadeIn(500);
	}
}, {
	offset: function() {
		return $('#introduction span').height();
	}
});

// Going up
$('#introduction').waypoint(function(direction) {
	if (direction === "up") {
		// Remove active class
		$('#my-collapsed-menu').children().children().removeClass();
		// Add proper active class
		$('a[href$="introduction"]').parent().addClass(navBarActiveCSSClass);
	}
}, {
	offset: function() {
		return $('#introduction span').height() - waypointOffset;
	}
});


// --------------------------------------------------------------------------
// Waypoints for contact

// Going down
$('#contact-content').waypoint(function(direction) {
	if (direction === "down") {
		// Remove active class
		$('#my-collapsed-menu').children().children().removeClass();
		// Add proper active class
		$('a[href$="contact"]').parent().addClass(navBarActiveCSSClass);
	}
}, {
	offset: function() {
		return 2*waypointOffset;
	}
});

// ---------------------------------------------------------------------------------------------------------------------
// Bounce the proceed-arrow.
function proceed_trigger_distance() {
	return $(window).height() - $(window).height()/10;
}

$('#intro-proceed').waypoint(function(direction) {
	if (direction === "down") {
		$('#intro-proceed-arrow').effect("bounce", { distance: 30, times:3 }, 800);
	}
}, {
	offset: function() {
		return proceed_trigger_distance();
	}
});
