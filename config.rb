preferred_syntax = :scss
http_path = '/'
css_dir = 'assets/stylesheets'
sass_dir = 'assets/scss'
images_dir = 'assets/images'
javascripts_dir = 'assets/js'
relative_assets = true
line_comments = true

require 'ceaser-easing'
