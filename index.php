<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset='utf-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge, chrome=1'>
    <title>Chris Aslanoglou</title>
    <meta name='description' content='Personal web page of Chris Aslanoglou. '>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <link href='favicon.ico' rel='icon'>
    <link rel='stylesheet' href='assets/stylesheets/normalize.css'>
    <link rel='stylesheet' href='assets/stylesheets/bootstrap.min.css'>
    <link href='//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet'>
    <link rel="stylesheet" href="assets/stylesheets/pictonic.css">
    <!--[if lt IE 8]>
    <script src="assets/js/pictonic.min.js"></script><![endif]-->
    <link rel="stylesheet" href="assets/stylesheets/font-mfizz.css">
    <link rel='stylesheet' href='assets/stylesheets/main.css'>

    <script src='assets/js/vendor/modernizr-2.6.2.min.js'></script>
</head>
<body>
<div class="fluid-container"><!---->
    <!-- <div class="container"> -->
    <nav class="navbar navbar-default navbar-fixed-top my-navbar" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#my-collapsed-menu">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="my-navbar-brand-list">
                    <li class="active">
                        <a class="scroll navbar-brand" href="#welcome">Chris Aslanoglou</a>
                    </li>
                </ul>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="my-collapsed-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#introduction" class="scroll">HOME</a></li>
                    <li><a href="#contact" class="scroll">CONTACT</a></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
    <!--[if lt IE 7]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
        your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="jumbotron my-jumbo" id="welcome">
        <div class="text-center" id="content">
            <span class="welcome-msg-title">Hey there, I'm Chris!</span>
            <p class="welcome-msg">
                Thanks for dropping by! If you'd like to learn more about me,
                click the arrow below.
            </p>
            <div class="attention proceed-arrow text-center">
                <a class="scroll" href="#introduction" id="jumbo-arrow">
                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </div>

    <!-- Introduction -->
    <div class="title text-center" id="introduction">
        <span><i class="fa fa-user" aria-hidden="true"></i> A few words about me</span>
    </div>
    <section class="container main-content" id="introduction-content">
        <p>
            My name is Chris Aslanoglou and I am a graduate
            student in the <a href="http://www.di.uoa.gr/eng/postgraduate/eng_specialization_3"
                              title="Link to Computer Systems specialization.">Computer Systems specialization</a>, at
            <a href="http://www.di.uoa.gr/eng" title="Link to Department of Informatics &amp; Telecommunications."
               rel="nofollow">Department of Informatics &amp; Telecommunications</a> of
            <a title="Link to University of Athens" href="http://en.uoa.gr/" rel="nofollow">University of Athens</a>.
            In September 2016, I received my B.Sc. in Computer Science with a specialization in Software, also in <span
                class="abbreviation" data-toggle="tooltip" data-placement="top"
                title="Department of Informatics &amp; Telecommunications, forgot already?">D.I.T.</span>.
        </p>
        <p class="top25">
            Below is a list (not exhaustive) of Computer Systems areas, that
            I find very interesting:
        </p>
        <!-- Interesting computer systems areas -->
        <div class="visible-lg visible-md visible-sm">
            <div class="row top10">
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <ul class="textAlignLeft">
                        <li>
                            <span class="icon-dbs-riak"></span>
                            <span>Distributed Systems</span>
                        </li>
                        <li>
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                            <span>Operating Systems</span>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6">
                    <ul class="textAlignRight">
                        <li>
                            <i class="fa fa-database" aria-hidden="true"></i>
                            <span>Big Data</span>
                        </li>
                        <li>
                            <span class="pictonic icon-vc-git"></span>
                            <span>Software Engineering</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="visible-xs">
            <div class="row textLeftAlign top10">
                <div class="col-xs-12">
                    <ul class="marginLeft10 textAlignLeft">
                        <li>
                            <span class="pictonic icon-dbs-riak"></span>
                            <span>Distributed Systems</span>
                        </li>
                        <li>
                            <i class="fa fa-cogs" aria-hidden="true"></i>
                            <span>Operating Systems</span>
                        </li>
                        <li>
                            <i class="fa fa-database" aria-hidden="true"></i>
                            <span>Big Data</span>
                        </li>
                        <li>
                            <span class="pictonic icon-vc-git"></span>
                            <span>Software Engineering</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Pass-times -->
        <p class="top15">
            While, some fun ways to pass time are:
        </p>
        <div class="visible-lg visible-md visible-sm">
            <div class="row top10">
                <div class="col-lg-6 col-md-5 col-sm-5">
                    <ul class="textAlignLeft">
                        <li>
                            <span class="pictonic icon-android"></span>
                            <span>Android Applications</span>
                            <br/>
                            <span class="li-content"></span>
                        </li>
                        <li>
                            <i class="icon-raspberrypi"></i>
                            <span>Raspberry Pi projects</span>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-7 col-sm-7">
                    <ul class="textAlignRight">
                        <li>
                            <span class="pictonic icon-prog-java"></span>
                            <span>JavaEE Applications development</span>
                            <br/>
                            <span class="li-content">JavaServerFaces, JAX-RS and JAX-WS</span>
                        </li>
                        <li>
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            <span>Web design</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="visible-xs">
            <div class="row textLeftAlign top10">
                <div class="col-xs-12">
                    <ul class="marginLeft10 textAlignLeft">
                        <li>
                            <span class="pictonic icon-android"></span>
                            <span>Android Applications</span>
                            <span class="li-content"></span>
                        </li>
                        <li>
                            <span class="pictonic icon-prog-java"></span>
                            <span>JavaEE Applications development</span>
                            <br/>
                            <span class="li-content marginLeft39">JavaServerFaces, JAX-RS and JAX-WS</span>
                        </li>
                        <li>
                            <span class="pictonic icon-vc-git"></span>
                            <span>Raspberry Pi projects</span>
                        </li>
                        <li>
                            <i class="fa fa-pencil" aria-hidden="true"></i>
                            <span>Web design</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="proceed-arrow text-center" id="intro-proceed">
            <div id="intro-proceed-arrow">
                <a class="scroll" href="#contact">
                    <i class="fa fa-angle-double-down" aria-hidden="true"></i>
                </a>
            </div>
        </div>
    </section>

    <!-- Contact me -->
    <div class="title text-center" id="contact">
        <span><i class="fa fa-comments" aria-hidden="true"></i> How to reach me</span>
    </div>
    <section class="container main-content" id="contact-content">
        <div class="row">
            <p class="col-xs-12 col-md-12 col-sm-12">
                Feel free to get in touch with me, I'll be more than happy to chat.<br/>
                You could either shoot me an email· the username is <br class="visible-xs-block"/>
                <span class="email-info">chris<i class="fa fa-circle" id="email-dot" aria-hidden="true"
                                                 data-toggle="tooltip" data-placement="top"
                                                 title="Yes, that's a dot"></i>aslanoglou</span>,
                and my mail provider is <span class="email-info">Google</span>.
                Or, you could also follow me on your favourite social
                network:
            </p>
        </div>
        <div class="row top30">
            <div class="col-md-2 col-md-offset-3 col-xs-2 col-xs-offset-2 col-sm-2 col-sm-offset-3 reach-me-here">
                <a href="https://twitter.com/chrisaslanoglou" alt="@chrisaslanoglou">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-md-2 col-xs-2 col-sm-2 reach-me-here">
                <a
                    href="https://www.linkedin.com/in/christosaslanoglou">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
            </div>
            <div class="col-md-2 col-xs-2 col-sm-2 reach-me-here">
                <a href="/blog" alt="a place of my own"><i class="fa fa-wordpress"></i></a>
            </div>
        </div>
        <div class="row top50">
            <p class="col-xs-12 col-md-12 col-sm-12">
                Feel free to browse through my <i>(soon to be updated)</i> repositories:
            </p>
        </div>
        <div class="row top30">
            <div class="col-md-4 col-md-offset-3 col-xs-4 col-xs-offset-2 col-sm-4 col-sm-offset-3 reach-me-here">
                <a href="https://github.com/chris-asl"><i class="fa fa-github"></i></a>
            </div>
            <div class="col-md-4 col-xs-4 col-sm-4 reach-me-here">
                <a href="https://bitbucket.org/chris_asl"><i class="fa fa-bitbucket"></i></a>
            </div>
        </div>
    </section>

    <div class="follow-scroll text-center">
        <a class="scroll hidden-xs" href="#welcome" data-toggle="tooltip" data-placement="top"
           title="Going up?">
            <i class="fa fa-angle-double-up"></i>
        </a>
    </div>
    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <?php $last_update_date = "8/2016"; ?>
            <span class="hidden-xs">Design by Chris Aslanoglou | Last update: <?php echo $last_update_date ?></span>
            <span class="visible-xs">Design by Chris Aslanoglou<br/>Last update: <?php echo $last_update_date ?></span>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
        integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
        crossorigin="anonymous">
</script>
<script src="assets/js/vendor/waypoints.min.js"></script>
<script src="assets/js/plugins.js"></script>
<script src="assets/js/vendor/bootstrap.min.js"></script>
<script src="assets/js/main.js"></script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-42440163-2', 'auto');
    ga('send', 'pageview');

</script>
</body>
</html>
